let api = require('express').Router();

api.get('/get', (req, res) => {
    res.json({ result: 'ok' });
});

api.post('/post', (req, res) => {
    res.json({ result: 'ok' });
});

let Firebase = require('../utils/firebase')
api.all('/fcm', (req, res) => {
    let topic = req.body.topic || req.query.topic
    let token = req.body.token || req.query.token
    let title = req.body.title || req.query.title
    let message = req.body.message || req.query.message
    if (title && message) {
        let image = req.body.image || req.query.image
        let ndata = typeof req.body.data == 'object' ? req.body.data : JSON.parse(req.body.data) || null
        let life = req.body.life || req.query.life
        let badge = req.body.badge || req.query.badge
        Firebase.send(topic, title, message, image, ndata, (data) => {
            let msg = { status: 'success', send_time: new Date().toLocaleString('zh', { hour12: false }), message_id: data.message_id }
            console.log(JSON.stringify(msg) + ' from ' + req.ip)
            res.json(msg);
        }, life, badge, token)
    } else {
        let msg = { status: 'failure', message: 'lose parameter topic or title or message' }
        console.log(JSON.stringify(msg) + ' from ' + req.ip)
        res.json(msg)
    }
});

api.post('/fcm2', (req, res) => {
    let topic = req.body.topic || req.query.topic
    let token = req.body.token || req.query.token
    let title = req.body.title || req.query.title
    let message = req.body.message || req.query.message
    if (title && message) {
        let image = req.body.image || req.query.image
        let ndata = typeof req.body.data == 'object' ? req.body.data : JSON.parse(req.body.data) || null
        let life = req.body.life || req.query.life
        let badge = req.body.badge || req.query.badge
        Firebase.send2(topic, title, message, image, ndata, (data) => {
            let msg = { status: 'success', send_time: new Date().toLocaleString('zh', { hour12: false }), message_id: data.message_id }
            console.log(JSON.stringify(msg) + ' from ' + req.ip)
            res.json(msg);
        }, life, badge, token)
    } else {
        let msg = { status: 'failure', message: 'lose parameter topic or title or message' }
        console.log(JSON.stringify(msg) + ' from ' + req.ip)
        res.json(msg)
    }
    res.json({ ok: true })
})

// api.get('/fcm03', (req, res) => {
//     Firebase.send3()
//     res.json({ ok: true })
// })

module.exports = api