const cors = require('cors')
const bodyParser = require('body-parser')
const session = require('express-session');
const multiparty = require('connect-multiparty');
const express = require('express');
const app = express();
const server = require('http').Server(app);

//proxy
app.set('trust proxy', 1) //trust first proxy
//跨域訪問
app.use(cors());
//解析body
app.use(bodyParser.json({ limit: '1024mb' }))
app.use(bodyParser.urlencoded({ limit: '1024mb', extended: true, parameterLimit: 10000 })) //巢狀資料結構
//session
app.use(session({
    resave: true,  // 新增
    saveUninitialized: true,  // 新增
    secret: 'recommand 128 bytes random string and number', // 使用長度128隨機字符
    cookie: { maxAge: 24 * 60 * 60 * 1000 } // 24 hr
}));
//上傳檔案
app.use(multiparty({ uploadDir: '/tmp' }))
//routes 
app.use((req, res, next) => {
    console.log(`[${new Date().toLocaleString('zh', { hour12: false })}] ${req.originalUrl} from ${req.ip}`)
    next();
});

app.get('/', (req, res) => {
    res.redirect('/index.html');
});

app.use('/', require('./routes/index'));
app.use('/', require('./routes/api'));

app.use('/', express.static(process.cwd() + '/public'));

app.use((req, res, next) => {
    console.error('server', req.originalUrl, req.ip)
    res.status(404).json({ error: 'Not Found' });
});
app.use((err, req, res, next) => {
    console.error('server', err.message, req.ip)
    res.status(500).json({ error: 'Something Error' });
});

server.listen(8080, '0.0.0.0', () => {
    console.log('server', `start running on ${server.address().address}:${server.address().port}`, 'server');
});