Util = {}
Util.get = (url, callback) => {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.onload = function () {
        let result = {}
        try {
            result = JSON.parse(xhr.responseText);
        } catch (error) {
            result.msg = xhr.responseText
        }
        return callback ? callback(result) : console[(xhr.readyState == 4 && xhr.status < "400") ? 'log' : 'error'](result);
    }
    xhr.onerror = function () {
        return callback ? callback() : console.error('ajax error')
    }
    xhr.send();
}

Util.post = (url, data) => {
    return fetch(url, {
        body: JSON.stringify(data || {}), // must match 'Content-Type' header
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, same-origin, *omit
        headers: {
            'user-agent': 'Mozilla/5.0 MDN Example',
            'content-type': 'application/json'
        },
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, cors, *same-origin
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // *client, no-referrer
    }).then(response => (response.status < 400) ? response.json() : response.text())
}