const request = require('request')
const cloud_msg_key = 'AIzaSyBkGRDsXWLT46VJsU-yq6ONxG7r3etEuJc'
const msg_life = 60 * 60 * 24

let Firebase = {};

// let dev_image = 'https://www.androidpolice.com/wp-content/uploads/2019/09/firebase.png'
// let dev_data = {
//     test: "fms",
//     number: 3
// }

let dev_image = null
let dev_data = null

Firebase.send = (topic, title, message, image, data, callback, life, badge, token) => {
    request({
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'key=' + cloud_msg_key,
        },
        uri: 'https://fcm.googleapis.com/fcm/send',
        body: JSON.stringify({
            condition: (topic && !token) ? `'${topic}' in topics` : null,
            to: token,
            content_available: true,
            time_to_live: msg_life * parseInt(life) || 1,
            notification: {
                title: title,
                body: message,
                image: image || dev_image,
                badge: badge,
                sound: 'default'
            },
            data: data || dev_data,
            priority: 'high'
        }),
        method: 'POST'
    }, function (err, res, body) {
        var obj = JSON.parse(body);
        if (callback) callback(obj);
    });
}

var admin = require('firebase-admin');
var serviceAccount = require("../assets/serviceAccountKey.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://cmcfcm-e771d.firebaseio.com"
});

Firebase.send2 = (topic, title, message, image, data, callback, life, badge, token) => {
    let payload = {
        notification: {
            title: title,
            body: message
        }
    }
    if (data) payload.data = data
    if (badge) {
        // payload.notification.badge = badge.toString()
        payload.apns = {
            headers: {
                "apns-priority": "5",
                "apns-collapse-id": "chatId"
            },
            payload: {
                aps: {
                    badge: parseInt(badge),
                    sound: "messageSent.wav",
                    "content-available": 1 // Double check this one if you are to actually use content-available or content_available for FCM
                }
            }

        }
    }
    if (image) payload.notification.image = image
    if (token) {
        // admin.messaging().sendToDevice(token, payload).then((d) => { callback(d) })
        payload.token = token
        admin.messaging().send(payload).then((d) => { callback(d) })
    } else {
        admin.messaging().sendToTopic(topic, payload).then((d) => { callback(d) })
    }
}

// var message = {
//     notification: {
//         title: '$GOOG up 1.43% on the day',
//         body: '$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.',
//     },
//     data: {
//         volume: "3.21.15",
//         contents: "http://www.news-magazine.com/world-week/21659772"
//     },
//     android: {
//         ttl:"4500s",
//         notification: {
//             icon: 'stock_ticker_update',
//             color: '#2233f2',
//         },
//         priority: "normal"
//     },
//     apns: {
//         payload: {
//             aps: {
//                 badge: 42,
//             },
//         },
//         headers: {
//             "apns-priority": "5",
//             "apns-expiration":"1604750400"
//         },
//     },
//     webpush: {
//         headers: {
//             Urgency: "high",
//             TTL:"4500"
//         }
//     },
//     topic: 'SYSTEM'
// };

// Firebase.send3 = () => {
//     admin.messaging().send(message)
//         .then((response) => {
//             // Response is a message ID string.
//             console.log('Successfully sent message:', response);
//         })
//         .catch((error) => {
//             console.log('Error sending message:', error);
//         });
// }

module.exports = Firebase;