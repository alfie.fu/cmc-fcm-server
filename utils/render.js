const fs = require('fs');

const DATA = '//data//'
const META = '<!-- meta -->'

let render = () => { }
render.file = (req, res, path, data, meta) => {
    if (!data) data = {}

    data['query'] = req.query

    let _data = [];
    for (var key in data) {
        var json = JSON.stringify(data[key]);
        while (json && json.match(/<\/?script[^>]*>/i)) {
            json = json.replace(/<\/?script[^>]*>/ig, '')
        }
        _data.push(`data.${key} = ${json};`);
    }
    try {
        let html = fs.readFileSync(process.cwd() + path, { encoding: 'utf8' });
        if (data) {
            html = html.replace(DATA, _data.join('\n\t\t'));
        }
        if (meta) {
            html = html.replace(META, `
            <meta property="og:title" content="${meta.title || '標題'}">
            <meta property="og:site_name" content="${meta.site_name || '首頁'}">
            <meta property="og:type" content="${meta.type || 'website'}">
            <meta property="og:locale" content="${meta.locale || 'zh_TW'}">
            <meta property="og:description" content="${meta.description || '描述'}">
            <meta property="og:url" content="${meta.url || 'https://www.google.com/'}">
            <meta property="og:image" content="${meta.image || 'https://picsum.photos/200/300/?random'}">
            `)
        }
        // 防止快取
        res.header("Cache-Control", "no-cache, no-store, must-revalidate");
        res.header("Pragma", "no-cache");
        res.header("Expires", 0);

        res.set('Content-Type', 'text/html');
        res.end(html);
    } catch (error) {
        console.error(error)
        res.status(500).json({ error: 'Server Error' })
    }
}

module.exports = render;